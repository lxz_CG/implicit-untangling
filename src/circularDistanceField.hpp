#pragma once
#include "utils.hpp"

struct circularDistanceField : public IU::scalarField{

    IU::vec3 m_center;
    float m_offset;

public:

    circularDistanceField(IU::vec3 center, float offset):
        m_center(center),
        m_offset(offset)
    {}

    float eval(const IU::vec3 &p){

        return m_center.distance(p)+m_offset;
    }

    IU::vec3 grad(const IU::vec3 &p){
        float dist=m_center.distance(p);

        if(dist>1e-5){
            return (m_center-p)/dist;
        } else {
            return IU::vec3(0,0,0);
        }
    }

};
