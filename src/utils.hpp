#pragma once

#include "lightEigen/Dense"
#include <iostream>
#include <fstream>

namespace IU //Implicit Untangling
{

typedef Eigen::VectorXf Vector;
typedef Eigen::MatrixXf Matrix;

struct vec3{
public:
    float x; float y; float z;
    vec3(float x, float y, float z): x(x),y(y),z(z){}
    vec3 operator +(vec3 v) const{
        return vec3(x+v.x,y+v.y,z+v.z);
    }
    vec3 operator -(vec3 v) const{
        return vec3(x-v.x,y-v.y,z-v.z);
    }
    vec3 operator *(float s) const{
        return vec3(x*s,y*s,z*s);
    }
    vec3 operator /(float s) const{
        return vec3(x/s,y/s,z/s);
    }

    float norm() const{
        return sqrt(x*x+y*y+z*z);
    }

    float distance (const vec3 &p) const{
        return (*this - p).norm();
    }
};


struct scalarField{
public:
    virtual float eval( const vec3 &p) = 0;
    virtual vec3 grad(const vec3 &p) = 0;
};

inline float sign(float f) {
    return (0<f) - (f<0);
}

//adaptation of https://gitlab.math.ethz.ch/NumCSE/NumCSE/commit/94fddbc5903e3b6c21b78a87ec607444c7071e1c
inline Matrix gramschmidtCol( const Matrix & A ) {
    Eigen::MatrixXf Q = A;
    // First vector just gets normalized
    Q.col(0).normalize();
    for(unsigned int j = 1; j < A.cols(); ++j) {
        // Replace inner loop over each previous vector in Q with fast matrix-vector multiplication
        Q.col(j) -= Q.leftCols(j) * (Q.leftCols(j).transpose() * A.col(j));
        // Normalize vector if possible (othw. means colums of A almsost lin. dep.
        if( Q.col(j).norm() <= 10e-14 * A.col(j).norm() ) {
            std::cerr << "Gram-Schmidt failed because A has lin. dep columns -> I don't know what's happening, try increasing/decreasing N ?\n" << std::endl;
            break;
        } else {
            Q.col(j).normalize();
        }
    }
    return Q;
}


typedef std::vector<std::vector< float> > grid;

inline void fillGrid(scalarField *f, grid &g, size_t resolution, float size){

    g.resize(resolution,std::vector<float>(resolution));
    for (size_t y = 0; y < resolution; ++y) {
        for (size_t x = 0; x < resolution; ++x){
            vec3 pos(((float)x-resolution/2.0f)*size/resolution,
                     ((float)y-resolution/2.0f)*size/resolution,
                     0.0f);
            g[x][y]=f->eval(pos);
        }
    }
}


//got this code from this answer on stack overflow  : https://stackoverflow.com/questions/2654480/writing-bmp-image-in-pure-c-c-without-other-libraries
inline void visuFieldFromGridBmp(grid gr,vec3 color,std::string filename){
    FILE *f;
    assert(gr.size()!=0 && "You're trying to visualize a grid of size 0");

    int w=gr.size();
    int h=gr[0].size();

    unsigned char *img = NULL;
    int filesize = 54 + 3*w*h;  //w is your image width, h is image height, both int

    int x,y,r,g,b;


    size_t numberNegativeIso = 2;
    float offsetNegativeIso = 0.3f;

    size_t numberPositiveIso = 2;
    float offsetPositiveIso = offsetNegativeIso;

    float fieldValue;

    img = (unsigned char *)malloc(3*w*h);
    memset(img,0,3*w*h);

    for(int i=0; i<w; i++)
    {
        for(int j=0; j<h; j++)
        {
            fieldValue=gr[i][j];

            vec3 colorValue=vec3(1,1,1) //writing in negative...
                    -(vec3(1,1,1)-color) * exp(-fieldValue*fieldValue*1000);
            for(size_t n=0;n<numberNegativeIso;n++){
                float fieldValueOffset = fieldValue + (n+1)*offsetNegativeIso;
                colorValue = colorValue - (vec3(1,1,1)-(color-vec3(0.2,0.2,0.2))) * exp(-fieldValueOffset*fieldValueOffset*1000);
            }
            for(size_t n=0;n<numberPositiveIso;n++){
                float fieldValueOffset = fieldValue - (n+1)*offsetPositiveIso;
                colorValue = colorValue - (vec3(1,1,1)-(color+vec3(0.2,0.2,0.2))) * exp(-fieldValueOffset*fieldValueOffset*1000);
            }

            r=255*colorValue.x;
            g=255*colorValue.y;
            b=255*colorValue.z;

            x=i; y=j;

            img[(x+y*w)*3+2] = (unsigned char)(r);
            img[(x+y*w)*3+1] = (unsigned char)(g);
            img[(x+y*w)*3+0] = (unsigned char)(b);

        }
    }

    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);

    f = fopen(filename.c_str(),"wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    for(int i=0; i<h; i++)
    {
        fwrite(img+(w*(h-i-1)*3),3,w,f);
        fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }

    free(img);
    fclose(f);
}

inline void visuAllFieldFromGridBmp(std::vector< grid> grids,std::string filename,std::vector<vec3> colors)
{

    FILE *f;

    int N=grids.size();

    assert(N!=0 && "You're trying to visualize 0 fields...");
    int w=grids[0].size();
    assert(w!=0 && "You're trying to visualize a grid of size 0...");
    int h=grids[0][0].size();

    unsigned char *img = NULL;
    int filesize = 54 + 3*w*h;  //w is your image width, h is image height, both int

    int r,g,b;

    float fieldValue;

    img = (unsigned char *)malloc(3*w*h);
    memset(img,0,3*w*h);

    for(int i=0; i<w; i++)
    {
        for(int j=0; j<h; j++)
        {

            fieldValue=grids[0][i][j];

            int indexShortestValue=0;
            for(int k=1;k<N;k++){
                float fieldValueTemp = grids[k][i][j];
                if(std::abs(fieldValueTemp)<std::abs(fieldValue)){
                    fieldValue=fieldValueTemp;
                    indexShortestValue=k;
                }
            }

            vec3 colorValue=vec3(1,1,1) //writing in negative...
                    -(vec3(1,1,1)-colors[indexShortestValue]) * exp(-fieldValue*fieldValue*1000);

            r=255*colorValue.x;
            g=255*colorValue.y;
            b=255*colorValue.z;

            img[(i+j*w)*3+2] = (unsigned char)(r);
            img[(i+j*w)*3+1] = (unsigned char)(g);
            img[(i+j*w)*3+0] = (unsigned char)(b);

        }
    }

    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);

    f = fopen(filename.c_str(),"wb");
    fwrite(bmpfileheader,1,14,f);
    fwrite(bmpinfoheader,1,40,f);
    for(int i=0; i<h; i++)
    {
        fwrite(img+(w*(h-i-1)*3),3,w,f);
        fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }

    free(img);
    fclose(f);

}

}
