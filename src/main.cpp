#include <iostream>

#include "circularDistanceField.hpp"
#include "implicituntangling.hpp"
#include <random>
#include <unistd.h>
#include <thread>

typedef std::vector<std::vector< float> >  grid;

void printHelp(){
    std::cout<<"\nUsage : implicitUntanglingDemo  [-n N -m -r reso -h]\n"<<
               "-n N (optional) : N is the number of implicit surfaces, ie of layers. Default is 2.\n"<<
               "-m (optional) : Indicates that multithreading should be used for big N.\n"<<
               "-r reso (optional) : reso is the size in pixels of outputed images. Default is 400. Increasing it increase quadratically the computation time,"
               " but might be useful for high N\n"<<
               "-h (optional) : shows help about usage, then halt the program\n";
}

int errorExit(){

    std::cerr<<"\nError : parsing of arguments failed \n";
    printHelp();
    return -1;

}

using namespace IU;

int main(int argc, char** argv){

    int N=2;
    bool multiThread = false;
    int resolutionVisu = 400;

    if(argc<2){
        printHelp();

        std::cout<<"\n\nContinuing with default values\n\n";

    } else {

        for(int i=1;i<argc;){
            char* arg = argv[i];
            if(strcmp(arg,"-n")==0){
                if(i+1>=argc){
                    return errorExit();
                }
                N=atoi(argv[i+1]);
                i+=2;
            }
            else if(strcmp(arg,"-m")==0){
                multiThread=true;
                i++;
            }
            else if(strcmp(arg,"-r")==0){
                if(i+1>=argc){
                    return errorExit();
                }
                resolutionVisu=atoi(argv[i+1]);
                i+=2;
            }
            else if(strcmp(arg,"-h")==0 || strcmp(arg,"-H")==0){
                printHelp();
                return 0;
            }
            else{
                return errorExit();
            }
        }
    }

    srand (time(NULL));

    std::vector<scalarField *> fields(N);
    std::vector<float> weights(N,1.0f);
    std::vector<float > thicknesses(N,0.1f);
    std::vector<vec3 > colors(N,vec3(0.,0.,0.));

    std::vector<grid> grids(N),correctedGrids(N);

    float radius=4;
    vec3 pos(0,0,0);

    //generating N nested implicit spheres, from the outer to the inner
    for(int i=0;i<N;i++){
        fields[N-i-1]=new circularDistanceField(pos,-radius);
        weights[N-i-1]=1+9*float(rand())/RAND_MAX;//some random weights
        thicknesses[N-i-1]=sqrt(weights[N-i-1])*0.04;//thicknesses depending on the weight
        colors[N-i-1]=vec3(0.3+0.5*float(rand())/RAND_MAX,0.3+0.5*float(rand())/RAND_MAX,0.3+0.5*float(rand())/RAND_MAX);//random color...

        float scale=0.8+ (float)rand()/RAND_MAX * 0.18;
        radius=scale*radius;
        float theta = rand();
        vec3 nextPos = pos + vec3(cos(theta),sin(theta),0)*scale*radius*0.4f;
        pos=nextPos;
    }

    weights[0]=N*5; //To ensure that the most inner layer doesn't purely disappear because of the addition of thicknesses (can happen with big N like 18),
                    //and to get nice round results :)

    //generating the corrected fields
    std::vector<scalarField *> fieldsCorrected(N);

    std::cout<<"Generating fields (computation of all operators, zero-sets, hyperplanes and adequate basis).\n";
    for(int i=0;i<N;i++){
        fieldsCorrected[i]=new CorrectedField(fields,weights,i,thicknesses);
    }
    std::cout<<"Fields generated.\n";

#ifdef __linux
    system("rm -rf output");
    system("mkdir output");
#else
    std::cout<<"Please, if there isn't, create a directory named output in the executable directory";
#endif

    std::cout<<"Visualizing results (we're looping over a uniform grid, this is long).\n";

    bool N_is_big = N>=5;

    if(multiThread && N_is_big){
        std::cout<<"N is big : Switching to multi-threading.\n";
        std::vector<std::thread> threads(N);
        for(int i=0;i<N;i++){
            threads[i]=std::thread(fillGrid,fields[i],std::ref(grids[i]),resolutionVisu,10);
        }
        for(int i=0;i<N;i++){
            threads[i].join();
        }
        for(int i=0;i<N;i++){
            visuFieldFromGridBmp(grids[i],colors[i],"output/f"+std::to_string(i+1)+".bmp");
        }
        visuAllFieldFromGridBmp(grids,"output/all_f.bmp",colors);
        std::cout<<"Initial fields visualisation outputed in output/f_*.bmp and in output/all_f.bmp\n";

        std::cout<<"Starting to output the corrected fields (this might take a while depending on N^3 and reso^2)\n";
        for(int i=0;i<N;i++){
            threads[i]=std::thread(fillGrid,fieldsCorrected[i],std::ref(correctedGrids[i]),resolutionVisu,10);
        }
        for(int i=0;i<N;i++){
            threads[i].join();
        }

        for(int i=0;i<N;i++){
            visuFieldFromGridBmp(correctedGrids[i],colors[i],"output/corrected_f"+std::to_string(i+1)+".bmp");
        }
        visuAllFieldFromGridBmp(correctedGrids,"output/all_corrected_f.bmp",colors);
        std::cout<<"All corrected_fields visualisation outputed in output/corrected_f_*.bmp and in output/all_corrected_f.bmp\n";
    }
    else
    {
        if(N_is_big)
            std::cout<<"\n !! N is big : You might consider switching to multi-threading (see implicitUntanglingDemo -h) !!\n\n";
        //////Visualising initial fields/////////
        for(int i=0;i<N;i++){
            fillGrid(fields[i],grids[i],resolutionVisu,10);
            visuFieldFromGridBmp(grids[i],colors[i],"output/f"+std::to_string(i+1)+".bmp");
        }
        visuAllFieldFromGridBmp(grids,"output/all_f.bmp",colors);
        std::cout<<"Initial fields visualisation outputed in output/f_*.bmp and in output/all_f.bmp\n";


        //////Visualising corrected fields/////////
        for(int i=0;i<N;i++){
            fillGrid(fieldsCorrected[i],correctedGrids[i],resolutionVisu,10);
            visuFieldFromGridBmp(correctedGrids[i],colors[i],"output/corrected_f"+std::to_string(i+1)+".bmp");
        }
        visuAllFieldFromGridBmp(correctedGrids,"output/all_corrected_f.bmp",colors);
        std::cout<<"Corrected fields visualisation outputed in output/corrected_f_*.bmp and in output/all_corrected_f.bmp\n";
    }


    std::cout<<"(This implementation is vulnerable to numerical instabilities.. \nIf the results feel wrong, try decreasing the weights differences between layers."
                "Big differences (such as weights ranging from 1 to 10000) and high N might create instability in the systems we solve during the creation of the fields.)\n";

    return 0;
}
